/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package hospitaladmissions;
import java.util.ArrayList;
/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public abstract class WristBand {

    String name;
    String dob;
    String doctor;
    String barcode;
    String medicalDetails;
    abstract public String getName();
    abstract public String getDOB();
    abstract public String getDoctor();
    abstract public String getBarcode();
    abstract public String getMedicalDetails();
    abstract public void setName(String name);
    abstract public void setDOB(String dOB);
    abstract public void setDoctor(String doctor);
    abstract public void setBarcode(String barcode);
    abstract public void setMedicalDetails(String details);
    abstract public String toString();
}
