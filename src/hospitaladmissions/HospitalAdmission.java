/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package hospitaladmissions;
import java.util.ArrayList;
/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class HospitalAdmission {
    public static void main(String[] args)
{
//initialize classes and add to the list
ResearchGroup rg=new ResearchGroup();
WristBand wb1=new BasicBand("John", "01/01/1990", "Shibi Mol", "A1A1A1A1", "medical details 1");
rg.add(wb1, 5);
WristBand wb2=new BasicBand("Dharsha", "05/03/1990", "Shibi Mol", "B1B1B1B1", "medical details 2");
rg.add(wb2, 15);
WristBand wb3=new BasicBand("Hepsa", "21/04/1990", "Shibi Mol", "C1C1C1C1", "medical details 3");
rg.add(wb3, 10);
WristBand wb4=new AlergicBand("Juby", "08/09/1990", "Shibi Mol", "D1D1D1D1", "medical details 4","Medicine1");
rg.add(wb4, 20);
WristBand wb5=new AlergicBand("Anisha", "01/03/1990", "Shibi Mol", "E1E1E1E1", "medical details 5","Medicine2");
rg.add(wb5, 35);
WristBand wb6=new AlergicBand("Jelin", "01/07/1990", "Shibi Mol", "F1F1F1F1", "medical details 6","Thomas");
rg.add(wb6, 25);
WristBand wb7=new AlergicBand("Annie", "05/07/1990", "Shibi Mol", "G1G1G1G1", "medical details 7","Shylin");
rg.add(wb7, 30);
ArrayList al=rg.getAdmissionDetails();
int time[]=rg.getWaitingTime();
//print the details of patients and their waiting time.
for(int i=0;i<al.size();i++)
{
System.out.println(al.get(i).toString());
System.out.println("Waiting Time:"+time[i]);
}
}
}
