/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package hospitaladmissions;

/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class BasicBand extends WristBand{
    BasicBand(String name, String dOB, String doctor, String barcode, String medicalDetails)
    {
    setName(name);
    setDOB(dOB);
    setDoctor(doctor);
    setBarcode(barcode);
    setMedicalDetails(medicalDetails);
    }
    //print details
    public String toString()
    {
    String str="\nType: Basic\nName: "+getName()+"\nDate Of Birth: "+getDOB()+"\n Family Doctor: "+getDoctor()+"\nBarcode: "+getBarcode();
    return str;
    }
    //define getter methods
    public String getName()
    {
    return name;
    }
    public String getDOB() 
    {
    return dob;
    }
    public String getDoctor() 
    {
    return doctor;
    }
    public String getBarcode() 
    {
    return barcode;
    }
    public String getMedicalDetails()
    {
    return medicalDetails;
    }
    //define setter methods
    public void setName(String name) 
    {
    super.name=name;
    }
    public void setDOB(String dOB) 
    {
    super.dob=dOB;
    }
    public void setDoctor(String doctor) 
    {
    super.doctor=doctor;
    }
    public void setBarcode(String barcode) 
    {
    super.barcode=barcode;
    }
    public void setMedicalDetails(String details) 
    {
    super.medicalDetails=details;
    }
    
}
