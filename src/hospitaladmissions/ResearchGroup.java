/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package hospitaladmissions;
import java.util.ArrayList;
/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class ResearchGroup {
    int waitingTime[]=new int[10];
int count=0;
//initialize array list
ArrayList<WristBand> admission=new ArrayList<WristBand>();
//return admission details
public ArrayList getAdmissionDetails()
{
return admission;
}
//return waiting time 
public int[] getWaitingTime()
{
return waitingTime;
}
//add to array list
public void add(WristBand band, int time)
{
if(count==waitingTime.length)
{
enlarge();
}
admission.add(band);
waitingTime[count]=time;
count+=1;
}
//remove from list
public void remove(String barcode)
{
for(int i=0;i<admission.size();i++)
{
if(admission.get(i).getBarcode().equals(barcode))
{
admission.remove(i);
break;
}
}
}
//enlarge the waiting time array size
private void enlarge()
{
int[] temp=new int[waitingTime.length];
for(int i=0;i<waitingTime.length;i++)
{
temp[i]=waitingTime[i];
}
waitingTime=new int[temp.length*2];
for(int i=0;i<temp.length;i++)
{
waitingTime[i]=temp[i];
}
}
}
